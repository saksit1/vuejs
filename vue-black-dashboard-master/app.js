const express = require('express') 
const mysql = require('mysql') 
const db = mysql.createConnection({   
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'music'
})
db.connect() 
const app = express()
var bodyParser = require('body-parser');
var urlencodeParser = bodyParser.urlencoded({ extended: false });
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(function (req, res, next) {
    res.set('Access-Control-Allow-Origin', '*');
    res.set('Access-Control-Allow-Methods', 'GET, POST, DELETE, PUT');
    res.set('Access-Control-Allow-Headers', 'Origin, Accept, Content-type, X-Requested-With, X-CSRF-Token');
    next();
})

// Select Data
app.get('/branch', (req, res) => {   // Router เวลาเรียกใช้งาน
    let sql = 'SELECT * FROM branch'  // คำสั่ง sql
   db.query(sql, (err, results) => { // สั่ง Query คำสั่ง sql
        if (err) throw err  // ดัก error
        console.log(results) // แสดงผล บน Console 
        res.json(results)   // สร้างผลลัพธ์เป็น JSON ส่งออกไปบน Browser
    })
})
app.post('/usersCreate',(req,res) => {
   
    let sql = "INSERT INTO users (username , password )  VALUES ('"+req.body.username+"','"+req.body.password+"');"
    let query = db.query(sql, (err, results) => { // สั่ง Query คำสั่ง sql
        if (err) throw err  // ดัก error
        console.log(results) // แสดงผล บน Console 
        res.json(results)   // สร้างผลลัพธ์เป็น JSON ส่งออกไปบน Browser
    })
})

app.post('/usersUpdate',(req,res) => {
   
    let sql = "UPDATE users SET username = '"+req.body.username+"',password ='"+req.body.password+"' WHERE  ID = '"+req.body.id+"';"
    
    
    let query = db.query(sql, (err, results) => { // สั่ง Query คำสั่ง sql
        if (err) throw err  // ดัก error
        console.log(results) // แสดงผล บน Console 
        res.json(results)   // สร้างผลลัพธ์เป็น JSON ส่งออกไปบน Browser
    })
})

app.post('/usersDelete',(req,res) => {
   
    let sql = "DELETE  FROM users WHERE ID = '"+req.body.id+"';"
    
    
    let query = db.query(sql, (err, results) => { // สั่ง Query คำสั่ง sql
        if (err) throw err  // ดัก error
        console.log(results) // แสดงผล บน Console 
        res.json(results)   // สร้างผลลัพธ์เป็น JSON ส่งออกไปบน Browser
    })
})
app.post('/usermusic',(req,res) => {
   
    let sql = "INSERT INTO userMusic (name , lastName ,birthday)  VALUES ('"+req.body.name+"','"+req.body.lastName+"','"+req.body.birthday+"');"
    let query = db.query(sql, (err, results) => { 
        if (err) throw err
        console.log(results)
        res.json(results)   
       
    })
})


app.listen('3000', () => {     // 
    console.log('start port 3000')
})
